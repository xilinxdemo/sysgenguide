Half adder
==========

Aim
---

* Create and simulate SysGen project
* Generate HDL netlist and check Vivado simulation results
* Create Cosimulation block and run the desing with it. 
* Create subblock and use it in larger design

Click below to download the design files, 

* :download:`half_adder.slx <designs/half_adder.slx>`
* :download:`full_adder.slx <designs/full_adder.slx>`
* :download:`half_adder_cosim_block.slx <designs/half_adder_cosim_block.slx>`
* :download:`fa_ethernet.slx <designs/fa_ethernet.slx>`

Create model
------------

* First create model as shown in Fig. :numref:`fig_1`, 
* Run the simulation to see the outputs. 


.. _`fig_1`:

.. figure:: img/1.jpg
   :width: 75%

   Half adder


* To create a subsystem for the half adder, select the half adder design and press ctrl-G as shown in :numref:`fig_2` 

.. _`fig_2`:

.. figure:: img/2.jpg
   :width: 75%

   Create block for the half adder

* Next double click on the subsystem and modify the port names as shown in :numref:`fig_3`, 
  

.. _`fig_3`:

.. figure:: img/3.jpg
   :width: 75%

   Modify input and output ports' name for the block


* After above settings, the final design will look as shown in :numref:`fig_4`, 
  
.. _`fig_4`:

.. figure:: img/4.jpg
   :width: 75%

   Final design for half adder 


Generate HDL netlist
--------------------

Follow the below steps to create the VHDL/Verilog design from the SysGen, 

* click on the System Generator token, and select the correct Board from the list. 
* Next, select "HDL netlist" from the compilation tab. 
* Also, select the create testbench and create interface document as shown in :numref:`fig_5`, and press generate button. 
* A vivado project will be created inside the folder 'netlist/hdl_netlist'
 
.. _`fig_5`:

.. figure:: img/5.jpg
   :width: 75%

   Generate HDL netlist

Simulate the HDL design
-----------------------

* Now open the project file from the folder "netlist/hdl_netlist". 
* Click on 'run simulation' and the simulation results will be shown as shown in :numref:`fig_6`, 

.. _`fig_6`:

.. figure:: img/6.jpg
   :width: 75%

   Simulation results for generated HDL netlist project


Hardware Cosimulation (JTAG)
----------------------------

* Generate the testbench file i.e. a matlab file will be generated. 
* Connect the board to the computer. 
* Run the matlab file to verify the results.
* Outputs are cleared after executing the matlab file. If we want to see the cosimulation outputs, then we need to add breakpoint in the matlab-file.

* Also a .slx file will be generated i.e. <project name>_hwcosim_lib.slx as shown in :numref:`fig_7`.
  
.. _`fig_7`:

.. figure:: img/7.jpg
   :width: 40%

   Hardware cosimulation block generated by SysGen

* Create a new model as shown in :numref:`fig_8`. Connect the board to the computer and run the simualtion. 

.. _`fig_8`:

.. figure:: img/8.jpg
   :width: 75%

   Use Hardware cosimulation block for simuation 


Create full adder using half adder
----------------------------------

* Open a new simulink file and save as 'full_adder.slx'. 
* Next, drag the exiting 'half_adder' block to this new file. 
* Now make connections to create full adder as shown in Fig. :numref:`fig_9`; and simulate the design. 


.. _`fig_9`:

.. figure:: img/9.jpg
   :width: 75%

   Full adder using Half adder


* Next create a subsystem for the full adder (i.e. select all items related to full adder and press ctrl-G) ; and modify the port names as shown :numref:`fig_10`. Finally modify the port names in top level design as shown in :numref:`fig_11`. 

.. _`fig_10`:

.. figure:: img/10.jpg
   :width: 75%

   Create subsystem for full adder

.. _`fig_11`:

.. figure:: img/11.jpg
   :width: 75%

   Modify port names in the top level design


* Now we can generate the netlist or IP etc. using System Generator block and see the simulation results for full adder as well. 


Hardware Cosimulation (Ethernet)
--------------------------------

In this section, we will use Ethernet connection for harware cosimulation. 


* First, generate the cosimulation block of "full adder" for "Ethernet" as shown in :numref:`fig_ethernet_sysgen`

.. _`fig_ethernet_sysgen`:

.. figure:: img/ethernet_sysgen.jpg
   :width: 75%

   Compilation "Harware cosimulation" for VC707 board

* Next, create the design as shown in :numref:`fig_ethernet_cosim_block`
  
.. _`fig_ethernet_cosim_block`:

.. figure:: img/ethernet_cosim_block.jpg
   :width: 75%

   Full adder using cosimulation block

.. * Now, connect the board to FPGA and see the results. 
  

.. Single step and free running mode
.. ---------------------------------

.. In the :numref:`ethernet_sysgen`, the clocking source is selected as 'single stepped' (default option). 
.. Now change the clock source to "free running" as shown in :numref:`free_running_e`, 

.. .. _`fig_free_running_e`:

.. .. figure:: img/free_running_e.jpg
..    :width: 75%

..    Clock source : free running




.. Questions
.. ---------

.. 1. how to send vector as input, so that simulation will show all combination of inputs and outputs. 
.. 2. How to use the generated HDL netlist to SysGen again. Error : only one entity declaration is allowed. 

.. .. code-block:: shell   

..     File used : 

..     netlist\hdl_netlist\half_adder.srcs\sources_1\imports\sysgen\half_adder.vhd