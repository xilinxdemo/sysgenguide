Black box in Sysgen
===================


Aim
---

Half adders are created using HLS and Verilog.
Then these two designs are imported in Sysgen to implement the full adder. 


* Click here to :download:`dowload <designs/hls_rtl.zip>` the design file.
* 'HLS project' and 'verilog' codes are inside the zip file. 
* Open 'hls_rtl.slx' and execute it to see the results. 

