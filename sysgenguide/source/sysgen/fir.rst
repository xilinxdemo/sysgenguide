FIR Filter
==========


Aim
---


* Create a Low pass filter using FIR filters and FDATools
* See the effect of sampling rates on the design. 

In this project, two sine waves of diffrent frequencies are added together and sent to the low pass filter (LPF). The LPF then pass only one frequency at the output and block the higher frequency. 

Click here to :download:`dowload <designs/sine_fir.slx>` the design file. 



Filter design
-------------

* First create the design as shown in :numref:`fig_12` and :numref:`fig_13`. Do not fill the "Constant" values in :numref:`fig_13`; these will be filled with the help of FDATools. 
  
.. note:: 

   Do the following in :numref:`fig_13`,

   * Double click on "AddSub" blocks and set the "Latency" to 0. 
   * Double click on "Mult" and "Delay" blocks and set "Latency" to 1.


.. _`fig_12`:

.. figure:: img/12.jpg
   :width: 75%

   Top level design


.. _`fig_13`:

.. figure:: img/13.jpg
   :width: 100%

   Sub-block design "FIR Type 1 Order 6"


* Now, double click on the FDATools and design the LPF as shown in :numref:`fig_14`. After fill the correct values, press the button "Design filter", 
  
.. _`fig_14`:

.. figure:: img/14.jpg
   :width: 75%

   LPF design using FDATools


* Next we need to store the filter coefficient values in the matlab workspace. Go "FDATools->File->Export" and fill the values as shown in :numref:`fig_15`. Note that filter coefficients are stored in the variable "filter_coeff". 

.. _`fig_15`:

.. figure:: img/15.jpg
   :width: 75%

   Export filter coefficients to matlab workspace

* Go to each constant block and import the filter coefficients using "filter_coeff(<location>)" as shown in :numref:`fig_16`. Do this for each constant block. The first block imports "filter_coeff(1)" and the last block will import filter_coeff(7). 

.. _`fig_16`:

.. figure:: img/16.jpg
   :width: 75%

   Import filter coefficients to constant blocks

* Set the frequency for both the sine wave blocks as "60 rad/sec" and "1884.95559 rad/sec" respectively. 

* Double click on the "Gateway In" block and set the sample period as 0.001. 

* Double click on SysGen token and set the 'Simulink clocking period' as 1/1000 in the 'Clocking' tab. 

* Finally, run the simulation. The four results in the :numref:`fig_17` are the "first sine wave", "second sine wave", "sum of sine waves" and "filtered output (i.e. sine wave with lower frequency)". 


.. _`fig_17`:

.. figure:: img/17.jpg
   :width: 100%

   Simulation results: only lower frequency wave is passed


* Since normalized frequency is used in the FDATools, therefore if we increase the sampling frequency e.g. 10000 Hz, the both the waveform will pass through the FIR filter. For this, change the 'Gateway In sample period' and 'Simulink clocking period' as '0.0001' and '1/10000' respectively. Now run the simulation again and we will see the sum of the sine waves in the output as both the frequencies are passed through LPF (see :numref:`fig_18`). 

.. _`fig_18`:

.. figure:: img/18.jpg
   :width: 100%

   Sum of sine wave is at output (due to decrease in sampling period)


  

  