.. Vivado documentation master file, created by
   sphinx-quickstart on Mon Sep 17 12:50:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Xilinx Demo
===========

.. toctree::
    :maxdepth: 3
    :numbered:
    :includehidden:
    :caption: Contents:

    sysgen/adder
    sysgen/fir
    sysgen/hls

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
