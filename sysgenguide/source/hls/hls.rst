

Adder
=====

.. code-block:: c
    :linenos:
    :caption: Half adder

    # include <stdio.h>
    #include <ap_cint.h>

    void half_adder(bool x, bool y, bool *sum, bool *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;
    }



.. code-block:: c
    :linenos:
    :caption: Half adder testbench

    # include <stdio.h>
    #include <ap_cint.h>

    void half_adder(bool, bool, bool*, bool*);

    int main(){
        bool sum, carry;
        half_adder(1, 1, &sum, &carry);
        printf("sum %d, carry %d", sum, carry);

        return 0;
    }


Full adder using half adder
===========================

.. code-block:: c
    :linenos:
    :caption: Full adder using half adder

    # include <stdio.h>
    #include <ap_cint.h>

    void half_adder(bool x, bool y, bool *sum, bool *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;
    }

    void full_adder(bool a, bool b, bool c, bool *sum, bool *carry){
        bool hf1_sum, hf1_carry; // sum and carry for half adder 1
        bool hf2_sum, hf2_carry;

        half_adder(a, b, &hf1_sum, &hf1_carry);
        half_adder(c, hf1_sum, &hf2_sum, &hf2_carry);

        *sum = hf2_sum;
        *carry = hf1_carry || hf2_carry;

    }


.. code-block:: c
    :linenos:
    :caption: Full adder testbench

    # include <stdio.h>
    #include <ap_cint.h>

    void full_adder(bool, bool, bool, bool*, bool*);

    int main(){
        bool sum, carry;

        full_adder(1, 1, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum, carry);

        full_adder(1, 0, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum, carry);

        return 0;
    }




